1. Clone Repo

git clone https://gitlab.com/davidsa/asyncupload.git

2. Npm install dependencies

cd asyncupload & npm install

3. Usage 

Get request to localhost:1234/upload - upload file form
Get request to localhost:1234/view - View auploaded file