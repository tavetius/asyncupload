const express = require('express');
const bodyParser = require('body-parser');
const file = require('express-fileupload');
const cons = require('consolidate');
const path = require('path');
const app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(file());

const EventEmitter = () => {
  const _listener = [];

  return {
    on: (listener) => {
      _listener.push(listener);
    },

    emit: (arg) => {
      _listener.map(listener => listener(arg));
    }
  }
};

const FileUploadEvent = EventEmitter();

app.get('/upload', function (req, res, next) {
  res.render('upload');
});

app.post('/upload', function (req, res, next) {
  if(req.files) {
    FileUploadEvent.emit(req.files.filename);
    res.send("Done");
  }
});

app.get('/view', function (req, res, next) {
  FileUploadEvent.on(function (file) {
    var view = new Uint8Array(file.data.length);
    for (var i = 0; i < file.data.length; ++i) {
      view[i] = file.data[i];
    }
    res.render('image_view', {image: view});
  });
});


let port = 1234;
app.listen(port, () => {
  console.log('Server is up and running on port numner ' + port);
});
